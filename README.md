# Testy

Użycie:

```sh
git clone git@gitlab.com:mimuw-rocznik-2001/jpp-2023/testy-zad1.git
# alternatywnie:
# git clone https://gitlab.com/mimuw-rocznik-2001/jpp-2023/testy-zad1.git

mv THTestPoly.hs original_THTestPoly.hs
ln -s testy-zad1/*.hs .
ghc THTestPoly.hs
./THTestPoly
``` 

### Docs

Przypominam, by nie zadawać pytań na forum jak nie trzeba. 
Istnieje (będzie istnieć jak ktoś stworzy) docs z listą pytań i odpowiedzi.
Link do niego jest gdzieś na grupie fb.

### Dodawanie swoich testów

Patrz [tutaj](https://gitlab.com/mimuw-ipp-2021/testy-duze-zadanie-3)
